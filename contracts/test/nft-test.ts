import { BigNumber } from '@ethersproject/bignumber';
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/dist/src/signer-with-address';
import { expect } from 'chai';
import { deployments, ethers, getNamedAccounts } from 'hardhat';
import _ from 'lodash';
import { FT } from './../typechain/FT.d';
import { NFT } from './../typechain/NFT.d';

// Tip: Chain state will be reset after each test !

const decoder = new TextDecoder("utf-8")

function expectEvent(result: any, matcher: object): any {
    expect(result).to.be.ok;
  const event: any = _.find(result.events, matcher)
  expect(event).to.be.ok
  // console.debug("expectEvent:", event)
  return event.args
}
function expectTransferEvent(result: any, to: string): any {
  const args = expectEvent(result, {event: "Transfer"})
  expect(args.from).to.equal(ethers.constants.AddressZero)
  expect(args.to).to.equal(to)
  const tokenId = args.tokenId;
  expect(tokenId).to.be.an.instanceOf(BigNumber)
  return args
}

async function claimNFT  (FT: FT, NFT: NFT, to: string) {
  await (await FT.approve(NFT.address, ethers.constants.WeiPerEther.mul(1))).wait()
  const claimResult = await (await NFT.claimFromFT(to)).wait();
  const { tokenId } = expectTransferEvent(claimResult, to)
  return tokenId
}

describe("NFT", function () {
  let deployer: string, signers: SignerWithAddress[], fooAcc: SignerWithAddress, FT: FT, NFT: NFT, lastMetaUpload: any
  before(async () => {
    lastMetaUpload = require('./../metadata/last-upload.json');

    deployer = (await getNamedAccounts()).deployer;
    signers = (await ethers.getSigners());
    fooAcc = signers[1];
    
    FT = await ethers.getContract('FT', deployer) as FT;
    NFT = await ethers.getContract('NFT', deployer) as NFT;
  });
  beforeEach(async () => {
    // This will init/reset contract state before each test
    await deployments.fixture(); 
  });

  it("Should return correct totalSupply", async function () {
    const totalSupply = (await NFT.totalSupply());
    expect(totalSupply).to.equal(0);
  });

  it("Should allow to claim from FT", async function () {
    const amount = ethers.constants.WeiPerEther.mul(1)

    console.debug("deployer FT balance", (await FT.balanceOf(deployer)).div(ethers.constants.WeiPerEther).toString())
    console.debug("FT totalSupply", (await FT.totalSupply()).div(ethers.constants.WeiPerEther).toString())

    const approvalResult = await FT.approve(NFT.address, amount)
    // console.debug("approval result", approvalResult)
    console.debug("NFT->FT allowance", (await FT.allowance(deployer, NFT.address)).toString())
    
    const claimResult = await (await NFT.claimFromFT(deployer)).wait();
    const { tokenId } = expectTransferEvent(claimResult, deployer)
    console.debug("claimFromFT resulting tokenID", tokenId)
    console.debug("ownerOf resulting tokenID", (await NFT.ownerOf(tokenId)))
    const tokenByIndex = (await NFT.tokenOfOwnerByIndex(deployer, 0));
    expect(tokenByIndex).to.equal(tokenId)
    await expect(NFT.tokenOfOwnerByIndex(deployer, 1)).to.be.reverted

    console.debug("deployer FT balance", (await FT.balanceOf(deployer)).div(ethers.constants.WeiPerEther).toString())
    console.debug("FT totalSupply", (await FT.totalSupply()).div(ethers.constants.WeiPerEther).toString())
    const balanceNFT = (await NFT.balanceOf(deployer));
    console.debug("deployer NFT balance", balanceNFT.toString())
    const totalSupply = (await NFT.totalSupply());
    console.debug("NFT totalSupply", totalSupply.toString())
    expect(balanceNFT).to.eq(BigNumber.from(1))
    expect(totalSupply).to.eq(BigNumber.from(1))
  });

  it("Should return correct tokenURI", async function () {
    for (let i = 0; i <= lastMetaUpload.artworkCount+1; i++) {
      const tokenId = await claimNFT(FT, NFT, deployer);
      const tokenURI = await NFT.tokenURI(tokenId);
      console.log("tokenURI", tokenId, tokenURI)
      expect(tokenURI).to.eq(
        `https://cloudflare-ipfs.com/ipfs/${lastMetaUpload.metaHash}/`
        + (i % lastMetaUpload.artworkCount)
        +'.json'
      )
    }
  });

  it("Should correctly flip to shipped", async function () {
    // claim 2 NFTs first
    const tokenIds = []
    for (let i = 0; i < 2; i++) {
      const tokenId = await claimNFT(FT, NFT, deployer);
      let shipped = await NFT.isShipped(tokenId)
      expect(shipped).to.be.false
      tokenIds.push(tokenId)
    }

    console.debug("convertToShipped:", tokenIds)
    const result = await NFT.convertToShipped(tokenIds)
    expect(result).to.be.ok

    for (const tokenId of tokenIds) {
      const shipped = await NFT.isShipped(tokenId)
      expect(shipped).to.be.true
    }
  });

  it("Should now allow to ship twice", async function () {
    // claim 2 NFTs first
      const tokenId = await claimNFT(FT, NFT, deployer);
      let shipped = await NFT.isShipped(tokenId)
      expect(shipped).to.be.false

    let result = await NFT.convertToShipped([tokenId])
    expect(result).to.be.ok

    // try again
    await expect(NFT.convertToShipped([tokenId])).to.be.reverted
  });

  it("Should return correct tokenURI for shipped", async function () {
    for (let i = 0; i <= 1; i++) {
      const tokenId = await claimNFT(FT, NFT, deployer);
      await NFT.convertToShipped([tokenId])
      const tokenURI = await NFT.tokenURI(tokenId);
      console.log("tokenURI", tokenId, tokenURI)
      expect(tokenURI).to.eq(
        `https://cloudflare-ipfs.com/ipfs/${lastMetaUpload.metaHash}/`
        + (i % lastMetaUpload.artworkCount)
        +'-shipped.json'
      )
    }
  });

  it("Should allow to change tokenBaseURI", async function () {
    await NFT.setTokenBaseURI('TEST/')
    const tokenId = await claimNFT(FT, NFT, deployer);
    await NFT.convertToShipped([tokenId])
    const tokenURI = await NFT.tokenURI(tokenId);
    console.log("tokenURI", tokenId, tokenURI)
    expect(tokenURI).to.eq(`TEST/` + (tokenId % 6) + '-shipped.json')
  });

  it("Should NOT allow to change tokenBaseURI for non-admin", async function () {
    // console.log({deployer, acc1})
    await expect(NFT.connect(fooAcc).setTokenBaseURI('HAXXOR/')).to.be.reverted
  });

  it("Should NOT allow to convertToShipped for non-owner", async function () {
    const tokenId = await claimNFT(FT, NFT, deployer);
    // console.log((await (await NFT.connect(fooAcc).convertToShipped([tokenId])).wait()))
    await (expect(NFT.connect(fooAcc).convertToShipped([tokenId])).to.be.reverted)
  });

  it("Should disallow claim after maxSupply", async function () {
    const maxSupply = (await NFT.maxSupply()).toNumber();
    console.log("Claiming", maxSupply, "NFTs")
    for (let index = 0; index < maxSupply; index++) {
      await claimNFT(FT, NFT, deployer);
    }
    await expect(claimNFT(FT, NFT, deployer)).to.be.reverted
  });
});
