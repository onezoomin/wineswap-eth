import { defineConfig } from 'vite'
// import reactRefresh from '@vitejs/plugin-react-refresh'
import envCompatible from 'vite-plugin-env-compatible'
import WindiCSS from 'vite-plugin-windicss'
// import polyfillNode from 'rollup-plugin-polyfill-node'
import react from '@vitejs/plugin-react'
import tsconfigPaths from 'vite-tsconfig-paths'
import svgrPlugin from 'vite-plugin-svgr'

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    port: 8080,
  },
  build: {
    // This changes the out put dir from dist to build
    // comment this out if that isn't relevant for your project
    outDir: 'build',
  },
  resolve: {
    alias: {
      process: 'process/browser',
      stream: 'stream-browserify',
      zlib: 'browserify-zlib',
      util: 'util',
    },
  },
  plugins: [
    // polyfillNode(), // using this https://github.com/vitejs/vite/issues/3817#issuecomment-864450199
    // reactRefresh(), // using below instead
    react({
      babel: {
        parserOpts: {
          plugins: ['decorators-legacy'],
        },
      },
    }),
    svgrPlugin({
      svgrOptions: {
        icon: true,
        // ...svgr options (https://react-svgr.com/docs/options/)
      },
    }),
    envCompatible({
      prefix: 'REACT_APP_',
    }),

    tsconfigPaths(),
    WindiCSS({ safelist: 'prose prose-sm m-auto' }),
  ],
})
