[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.tamera.org/cybernetics/uniwine)

# [uniwine](https://uniwine.exchange)

An experiment in dynamically priced wine.

## Setup

(When using Gitpod this is done automatically)

1. Copy `cp .env.example` to `.env`
2. Set RPC URL for rinkeby testnet
3. Run `yarn start:app`
