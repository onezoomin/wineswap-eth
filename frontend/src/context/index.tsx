import { Dispatch, SetStateAction, useCallback, createContext, useContext, useState } from 'react'
import { MODAL_TYPES } from '../utils/main-utils'

const initialState = {
  showConnect: false,
  visible: false,
  count: 1,
  noUpdate: false,
  valid: false,
  fullAllowance: true,
  tradeType: MODAL_TYPES.BUY,
  verbose: false,
  fake: false,
}
export type AppContextContent = typeof initialState

interface ContextType {
  appState: AppContextContent
  setAppState: Dispatch<SetStateAction<AppContextContent>>
  updateAppState: (change: object) => void
}

export const AppContext = createContext<ContextType>({
  appState: initialState,
  setAppState: (() => { throw new Error('AppContext not initialized') }) as () => {},
  updateAppState: (() => { throw new Error('AppContext not initialized') }) as () => {},
})

export function useAppContext (): ContextType {
  return useContext(AppContext)
}

export default function AppProvider ({ children }) {
  const [appState, setAppState] = useState<AppContextContent>(initialState)
  const updateAppState = useCallback(change => setAppState(s => ({ ...s, ...change })), [setAppState])

  return <AppContext.Provider value={{ appState, setAppState, updateAppState }}>
    {children}
  </AppContext.Provider>
}
