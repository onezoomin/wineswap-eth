import { useState } from 'react'
import { Link } from 'react-router-dom'
import { LinkOut } from '../../components/minis'
import discord from '../../img/discord.svg'
import medium from '../../img/medium-alt.svg'
import question from '../../img/question.svg'
import settings from '../../img/settings_black_24dp.svg'
import telegram from '../../img/telegram.svg'
import ws from '../../img/wineswap_logo.svg'
import { FullWidthBarClasses } from './index'
import SettingsModal from './SettingsModal'

export function Footer () {
  const [showSettings, setShowSettings] = useState(false)
  const FooterBarProps = {
    className: `${FullWidthBarClasses} p-4`,
  }
  const toggleShowSettings = () => {
    setShowSettings(!showSettings)
  }
  return (
    <div id="FullWidthBarFooter" {...FooterBarProps}>
      <div className="w-full flex justify-between align-baseline">
        <Link to="/">
          <img className="h-10" src={ws} alt="WineSwap Logo" />
        </Link>
        <div className="flex flex-row space-x-4 justify-end items-center">
          <Link to="/">
            <img className="h-6 bg-gray-50 p-0.5" src={medium} alt="medium Logo" />
          </Link>
          <Link to="/">
            <img className="h-6" src={telegram} alt="telegram Logo" />
          </Link>
          <LinkOut href="https://discord.gg/9yaDQBqMUF">
            <img className="h-5" src={discord} alt="discord Logo" />
          </LinkOut>

          <Link className="inline-block ml-2" to="/faq">
            <img className="h-5 -mb-0.5" src={question} alt="help / info button" />
          </Link>

          <img onClick={toggleShowSettings} className="h-6 cursor-pointer" src={settings} alt="Settings Icon" />

        </div>
      </div>

      <SettingsModal visible={showSettings} setVisible={setShowSettings} />
    </div>
  )
}
