import { Switch } from '@headlessui/react'
import { useEffect, useMemo, useState } from 'react'
import { ButtonTW } from '../../components/ButtonTW'
import { useAppContext } from '../../context'
import { n8nOrder } from '../../network/n8n'
import NavModal from './NavModal'

function SettingsToggle ({ value, setValue, children }) {
  return <div className="my-2">
    <Switch.Group>
      <Switch.Label className="text-gray-50 mr-4">{children}</Switch.Label>
      <Switch
        checked={value}
        onChange={setValue}
        className={`${value ? 'bg-blue-300' : 'bg-gray-700'}
          relative inline-flex flex-shrink-0 h-5 w-10 border-2 border-white rounded-full cursor-pointer
          transition-all ease-in-out duration-200 focus:outline-none focus-visible:ring-2  focus-visible:ring-white focus-visible:ring-opacity-75`}
      >
        <span className="sr-only">Use setting</span>
        <span
          aria-hidden="true"
          className={`${value ? 'translate-x-5' : 'translate-x-0'}
            pointer-events-none inline-block h-4 w-4 rounded-full bg-white shadow-lg transform ring-0 transition ease-in-out duration-200`}
        />
      </Switch>
    </Switch.Group>
  </div>
}

export default function SettingsModal ({ visible, setVisible }) {
  const [enabled, setEnabled] = useState(false)
  const { appState, updateAppState } = useAppContext()
  const [loaded, setLoaded] = useState(false)
  // Persist to & load from LocalStorage - TODO: use useLocalStorage?
  const persisted = useMemo(() => ({ fake: appState.fake, verbose: appState.verbose }), [appState.fake, appState.verbose])
  useEffect(() => {
    if (!loaded) {
      setLoaded(true)
      const savedString = localStorage.getItem('settings')
      const savedParsed = JSON.parse(savedString)
      console.debug('[Settings] Load from localStorage:', savedParsed)
      if (savedParsed) {
        updateAppState(savedParsed)
      }
    } else {
      console.debug('[Settings] Saving to localStorage:', persisted)
      localStorage.setItem('settings', JSON.stringify(persisted))
    }
  }, [persisted, loaded, setLoaded, updateAppState])
  return (
    <NavModal backNav={false} {...{ visible, setVisible }} className="flex flex-col">
      <SettingsToggle
        value={appState.fullAllowance}
        setValue={val => updateAppState({ fullAllowance: val })}
      >
        Full allowance
      </SettingsToggle>
      <SettingsToggle
        value={appState.fake}
        setValue={val => updateAppState({ fake: val })}
      >
        Fake transactions
      </SettingsToggle>
      <SettingsToggle
        value={appState.noUpdate}
        setValue={val => updateAppState({ noUpdate: val })}
      >
        No updating in BG
      </SettingsToggle>
      <SettingsToggle
        value={appState.verbose}
        setValue={val => updateAppState({ verbose: val })}
      >
        Verbose UI
      </SettingsToggle>
      <SettingsToggle
        value={enabled}
        setValue={setEnabled}
      >
        Set Ting
      </SettingsToggle>
      <ButtonTW onClick={() => n8nOrder()} >n8n</ButtonTW>
    </NavModal>
  )
}
