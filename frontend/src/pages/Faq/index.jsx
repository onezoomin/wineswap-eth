import { Disclosure } from '@headlessui/react'
import { MinusIcon, PlusIcon } from '@heroicons/react/solid'

import { LinkOut, TitleH2, N8nIcon } from '../../components/minis'
import gitlab from '../../img/gitlab-white-solid.png'
import media from '../../img/media-network.png'
import textile from '../../img/textile-white-solid.png'
import { appendClassNames } from '../../utils/react-utils'

const FaqContent = ({ children, className }) => (
  <div id="FaqContent" {...appendClassNames('w-full py-4', className)}>
    {children}
  </div>
)
const buttonClasses = 'flex justify-start w-full px-4 py-0 text-sm font-medium text-left text-gray-50 rounded-lg  focus:outline-none focus-visible:ring focus-visible:ring-gray-50 focus-visible:ring-opacity-75'
const panelClasses = 'px-4 pt-1 pb-2 text-sm text-gray-500'
const toggleClasses = 'w-5 h-5 mr-5 text-gray-50'

const DisclosureBar = ({ children, className = '', open = false }) => (
  <Disclosure.Button {...appendClassNames(buttonClasses, className)}>
    { open ? <MinusIcon className={toggleClasses} /> : <PlusIcon className={toggleClasses} /> }

    {children}
  </Disclosure.Button>)

const DisclosureContent = ({ children, className = '' }) => (
  <Disclosure.Panel {...appendClassNames(panelClasses, className)}>
    {children}
  </Disclosure.Panel>)
// const NOTION_SECRET = 'secret_A3RLpNXyWbwCRCjj6uVL4ZGoF4yd6GYsGEOpdDKcLKn'
// TODO consider api slurp from notion !
const FaqData = [
  ['Availability', (<>
    WineSwap offers <LinkOut href="https://blog.qtum.org/understanding-non-fungible-tokens-3e5770e3288f#1623">two kinds of tokens</LinkOut>:<br />
    - FTs which serve as vouchers, and<br />
    - NTFs which each have a specific artwork <br /> (only 6 copies of each piece)
  </>)],
  ['Wallet', 'Connect using a Web3 wallet like MetaMask'],
  ['Buy', 'Buy FTs from the pool'],
  ['Sell', 'Sell back FTs to the pool'],
  ['Claim', (<>Swap one VINO FT at a time for a VERITAS NFT<br /> (redeemable for a shipped bottle or tradeable)</>)],
  ['Trade', 'Click the opensea icon anywhere you see it to view and / or trade your VERITAS NFT on the open market'],
  ['Ship', 'Sign a message with your wallet, and convert the NFT to shipped. Then let us know via email or discord message and we\'ll send a bottle to your address'],
  ['TechStack', (<>
    WineSwap is deployed via ipfs and relies on the following services:<br />
    <div className="flex flex-row justify-start items-center mb-2">
      <img className="h-5 mr-3" src={gitlab} alt="media network Logo" />
      <p><LinkOut href="https://about.gitlab.com/">Gitlab</LinkOut> - self hosted git with CI to handle auto deployment</p>
    </div>
    <div className="flex flex-row justify-start items-center mb-2 ">
      <img className="h-5 mr-3" src={textile} alt="media network Logo" />
      <p><LinkOut href="https://textile.io/">Textile</LinkOut> - IPFS service with buckets</p>
    </div>
    <div className="flex flex-row justify-start items-center mb-2  ">
      <img className="h-5 mr-3" src={media} alt="media network Logo" />
      <p><LinkOut href="https://media.network/">Media Network</LinkOut> - Truly anonymous, blockchain-based CDN service</p>
    </div>
    <div className="flex flex-row justify-start items-center mb-2  ">
      <N8nIcon className="h-5 mr-3" alt="n8n  Logo" />
      <p><LinkOut href="https://n8n.io/">n8n</LinkOut> - FOSS Workflow Automation Tool</p>
    </div>
  </>)],
  ['More', (<>
    More details are available:<br />
    - Medium Post 1<br />
    - Medium Post 2<br />
  </>)],
]
export default function Faq ({ className }) {
  const anchor = window.location.href.split('#').pop()
  return (
    <FaqContent {...{ className }}>
      <TitleH2 m="4" className="-mt-6">
        FAQ
      </TitleH2>
      {FaqData.map((eachQ, idx) =>
        (<Disclosure defaultOpen={!!(anchor === eachQ[0])} key={`faq-${idx}`} as="div" className={`${idx > 0 ? 'pt-2' : ''}`}>
          {({ open }) => (
            <>
              <DisclosureBar {...{ open }}>
                <span>{eachQ[0]}</span>
              </DisclosureBar>

              <DisclosureContent>
                {eachQ[1]}
              </DisclosureContent>
            </>
          )}
        </Disclosure>),
      )}

    </FaqContent>
  )
}
