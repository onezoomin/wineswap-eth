import filter from 'lodash/filter'
import { useEffect, useMemo, useState } from 'react'
import { Redirect } from 'react-router-dom'
// import { useAppContext } from '../../context'
import { NFTPicker } from '../../components/NFTPicker'
import { useUniwineMeta } from '../../hooks/main'
import { useActiveWeb3React } from '../../hooks/web3'
import { torus } from '../../network/connectors'
import { amountFormatter, formatAddressShort, FT_NAME, NFT_NAME } from '../../utils/main-utils'

// const OrderDiv = ({ children }) => (
//   <div className="flex flex-col items-center mx-10% justify-between">
//     {children}
//   </div>
// )

const StatusContent = ({ children, className }) => (
  <div id="StatusContent" className={className + ' flex flex-col items-center mb-2 justify-between'}>
    {children}
  </div>
)

export default function Status ({ ethData, className }) {
  const { balanceWINE, balanceUNIWINE, tokensUNIWINE, UNIWINE } = ethData
  const { account, library } = useActiveWeb3React()
  const [error] = useState()

  const erosRedeemed = useMemo(() => filter(tokensUNIWINE, { shipped: true }), [tokensUNIWINE])
  const redeemedCount = erosRedeemed ? erosRedeemed.length : null
  const tokenMeta = useUniwineMeta(tokensUNIWINE)
  console.log({ tokensUNIWINE, erosRedeemed, redeemedCount })

  // show Torus button while this is open
  useEffect(() => {
    if (library?.provider) {
      if (library.provider.isTorus) {
        if (!torus.torus) {
          console.warn('torus.torus is', torus.torus)
        } else {
          // torus.torus?.showWallet()
          torus.torus?.showTorusButton()
          return () => torus.torus?.hideTorusButton()
        }
      }
    }
  }, [library.provider])

  if (!account) {
    return <Redirect to={'/'} />
  } else {
    return (
      <StatusContent className={className}>
        <span className="mb-8 text-gray-400">
          {formatAddressShort(account)}
        </span>

        <span className="text-4xl mb-3 tracking-wider">
          {balanceWINE ? amountFormatter(balanceWINE, 18, 0) : 0} {FT_NAME} <span className="text-gray-400  text-2xl">FT</span>
        </span>

        <span className="text-4xl tracking-wider">
          {balanceUNIWINE ? amountFormatter(balanceUNIWINE, 0, 0) : 0} {NFT_NAME} <span className="text-gray-400 text-2xl">NFT</span>
        </span>
        {redeemedCount
          ? <span className="text-xl mt-2 text-gray-400 tracking-wider">
            ({redeemedCount} ordered)
          </span>
          : null}
        {!balanceUNIWINE
          ? null
          : <NFTPicker
              tokens={tokensUNIWINE}
              tokenMeta={tokenMeta}
              tokenType={UNIWINE}
              highlighted={erosRedeemed.map(t => t.id)}
              selected={null}
              setSelected={null}
          />}

        {error && <p className="text-red-500">Error</p>}

      </StatusContent>
    )
  }
}
