
import { useLocation } from 'react-use'
import useAckee from 'use-ackee'
import Body from '../Body'

export default function Main ({ status, faq }) {
  // Ackee location
  const location = useLocation()
  useAckee(location.pathname, {
    server: 'https://wineswap-ackee.vercel.app',
    domainId: 'a64d13a5-96a8-4f3c-9413-a6d9888fe7aa',
  }, {
    ignoreLocalhost: false,
    ignoreOwnVisits: false,
    detailed: true,
  })

  return (
    <Body
      showStatus={status}
      showFAQ={faq}
    />
  )
}
