import axios from 'axios'
// import Moralis from 'moralis'
import { Process } from '../hooks/process'

export interface orderJson {
  signedMessage: string
  shippingAddress: string
  signature: string
  account: string
  nftList: number[]
  email?: string
}

async function post (url: string, body: any, options?: any) {
  try {
    const response = await axios.post(url, body, options)
    return response.data
  } catch (err) {
    throw new Error(err)
  }
}

const defaultOrder: orderJson = {
  signedMessage: // important that there are no extra spaces from indentation
`PLEASE VERIFY YOUR ADDRESS (physical and wallet)!
Your data is only sent to us and will never be shared publicly.

Jo Go
123 Overthere
7630 Reliq
PT
that@guy.com
ETH Wallet: 0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e
Timestamp: 2022-02-10T20:51:58.321Z
NFT IDs: 14`,
  shippingAddress:
`Jo Go
123 Overthere
7630 Reliq
PT
that@guy.com`,
  signature: '0x53696810ba9641d6db75fb9892d6361a99699cf10b351ad0f14980a33a3fba5658435ab91d95c7fae2bb5a5d11b678fec421c2604567b1887e2e21ee61aa20591c',
  account: '0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e',
  nftList: [14],
  email: 'that@guy.com',
}
export async function n8nOrder (orderObj: orderJson = defaultOrder, process?: Process) {
  let result
  if (!process) return console.log(await post('https://wh.n8n.zt.ax/webhook/order', orderObj)) // meant for testing only
  await process.trackStep({ type: 'submit', status: 'wait' }, async () => {
    result = await post('https://wh.n8n.zt.ax/webhook/order', orderObj)
    console.log('Moralis result:', result)
  })
  return result
}

// export async function moralisCreateOrder (account: string, signedMessage: string, userAddress: string, signature: string, nftList: number[], email: string, process: Process) {
//   let result
//   await process.trackStep({ type: 'submit', status: 'wait' }, async () => {
//     result = await Moralis.Cloud.run(
//       'handleShipping',
//       { signedMessage, shippingAddress: userAddress, signature, account, nftList, email },
//       { autoFetch: false },
//     )
//     console.log('Moralis result:', result)
//   })
//   return result
// }

// export async function moralisAddOrderNotion (account: string, signedMessage: string, signature: string, nftList: number[], email: string) {
//   const shippingAddress = signedMessage.split(/\n\n/).pop()

//   const result = await Moralis.Cloud.run(
//     'addOrderNotion',
//     { signedMessage, shippingAddress, signature, account, nftList, email },
//     { autoFetch: false },
//   )
//   console.log('Moralis result:', result)
// }

// export async function moralisJoinDiscord (authCode: string, redirectUri: string, account: string|nullish, moralisId = 'TODO') {
//   const result = await Moralis.Cloud.run(
//     'discordAuth',
//     { authCode, redirectUri, moralisId, ethAddress: account },
//   )
//   console.log('Moralis joinDiscord result:', result)
// }
