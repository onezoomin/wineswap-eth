
import { extendClasses } from '../../utils/react-utils'

/**
 * Takes in custom size and stroke for circle color, default to primary color as fill,
 * need ...rest for layered styles on top
 */
export default function Loader ({
  size = '16px',
  stroke = 'white',
  strokeWidth = '2.5',
  ...rest
}: {
  size?: string
  stroke?: string
  [k: string]: any
}) {
  return (
    <svg
      style={{
        height: size,
        width: size,
        animation: 'rotateLoader 2s linear infinite',
      }}
      viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" stroke={stroke} {...rest}
    >
      <path
        stroke={stroke}
        d="M12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22C17.5228 22 22 17.5228 22 12C22 9.27455 20.9097 6.80375 19.1414 5"
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
/**
 * Takes in custom size and stroke for circle color, default to primary color as fill,
 * need ...rest for layered styles on top
 */
export function LoaderOverlay ({ className = '', size = '60%', ...restProps }) {
  return (
    <div className={extendClasses(className, 'absolute inset-0 h-full w-full bg-black bg-opacity-80 flex items-center justify-center z-10')} {...restProps}>
      <Loader size={size} />
    </div>
  )
}
