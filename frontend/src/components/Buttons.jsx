import { BigNumber } from '@ethersproject/bignumber'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'
import { useSnackbar } from 'notistack'
import { useState } from 'react'
import { withRouter } from 'react-router-dom'
import OAuth2Login from 'react-simple-oauth2-login'
import { useCopyToClipboard } from 'react-use'
import { useEthContext } from '../context/eth-data'
import { useAppContext } from '../context/index'
import { useActiveWeb3React } from '../hooks/web3'
import discord from '../img/discord.svg'
import { useUIContext } from '../pages/Body/UIContext'
import { ackeeBuyFunnel, ackeeBuyHome, ackeeShipHome } from '../utils/ackee'
import { formatAddressShort, MODAL_TYPES } from '../utils/main-utils'
import { rIF } from '../utils/react-utils'
import { ButtonTW } from './ButtonTW'
import Loader from './Loader'
import { FlexRow } from './minis'

export const ConnectButton = withRouter(function ConnectButton ({ setShowConnect, history, buttonText = 'Wallet' }) {
  const { ready, balanceWINE } = useEthContext()
  const w3react = useActiveWeb3React()
  const { account } = w3react

  async function handleAccount () {
    if (!account) {
      // try {
      //   let res = await activate(injected, undefined, true)
      //   console.log(res);
      // } catch (error) {
      //   console.error("Injected connector error", error)
      //   if(error.code === "UNSUPPORTED_NETWORK") {
      //     console.log('Unsupported Network')
      //     alert("Unsupported network")
      //   }
      console.log(account)
      setShowConnect(true) // open connect modal
      // }
    } else {
      history.push('/status')
    }
  }

  const statusBg = account === null ? 'bg-red-500' : (ready ? 'bg-green-500' : 'bg-yellow-500')

  return (
    <ButtonTW id="Wallet"
      buttonType={account ? 'outline' : 'outline'}
      className={`min-w-max-content text-lg tracking-widest m-0 flex flex-row ${account ? 'normal-case' : ''}`}
      onClick={handleAccount}
    >
      {account
        ? formatAddressShort(account)
        : <p className="m-0">{buttonText}</p>
      }

      {balanceWINE && !ready
        ? (<div className={`w-3 h-3 ml-2 rounded-full ${statusBg}`} />)
        : null
      }

    </ButtonTW>
  )
})

export const DiscordButton = withRouter(function ConnectButton ({ _moralisPlacedOrderId, buttonText = 'Discord' }) {
  // const { account } = useActiveWeb3React()
  const { errorDialog } = useUIContext()
  const [pending, setPending] = useState(false)
  const [success, setSuccess] = useState(false)
  const publicUrl = window.location.origin
  const redirectUri = `${publicUrl}/#/oauth-callback/discord`

  const join = async ({ code }) => {
    try {
      setPending(true)
      // await mo ralisJoinDiscord(code, redirectUri, account, moralisPlacedOrderId)
      setSuccess(true)
    } catch (error) {
      errorDialog('Failed to join discord server', error)
    } finally {
      setPending(false)
    }
  }

  if (pending) return <Loader />
  if (success) return <em>Success, check Discord</em>

  return (
    <OAuth2Login
      className="py-1 px-2 relative flex justify-center items-center text-center uppercase rounded text-lg tracking-widest bg-transparent bg-opacity-0 text-white border-white border-2 border-solid hover:bg-black focus:bg-black hover:bg-opacity-30 focus:bg-opacity-30 transform duration-150 hover:scale-102 focus:scale-102"
      authorizationUrl="https://discord.com/api/oauth2/authorize" /* &permissions=0 */
      responseType="code"
      clientId={process.env.REACT_APP_DISCORD_APP_ID}
      redirectUri={encodeURIComponent(redirectUri)} /* https://github.com/bhubr/react-simple-oauth2-login/issues/22 */
      scope="identify guilds.join"
      onSuccess={join}
      onFailure={error => errorDialog('Failed to login to Discord', error)}
    >
      <img className="h-4" src={discord} alt="" />
      {rIF(buttonText, <p className="m-0 ml-2">{buttonText}</p>)}
    </OAuth2Login>
  )
})

export const ShipButtons = withRouter(function ConnectButton ({ signedMessage, moralisPlacedOrderId, onFinished /*, ethData, history */ }) {
  const [dialogVisible, setDialogVisible] = useState(false)
  const [dialogActionDone, setDialogActionDone] = useState(false)
  const [copyState, copyToClipboard] = useCopyToClipboard()
  const { enqueueSnackbar } = useSnackbar()

  const closeDialog = () => {
    setDialogVisible(false)
    setDialogActionDone(false)
  }

  // async function handleShip (service) {
  //   console.log('Ship send:', service, signedMessage)
  //   if (service === 'email') {
  //     if (!process.env.REACT_APP_ORDER_EMAIL) throw new Error('Missing env REACT_APP_ORDER_EMAIL')
  //     const link = mailtoLink({ to: process.env.REACT_APP_ORDER_EMAIL, subject: 'Uniwine Order', body: signedMessage })
  //     // https://stackoverflow.com/questions/10356329/mailto-link-multiple-body-lines#10356432
  //     // ! RFC says so, but 2 out of 3 tried email clients turn it into extra newlines
  //     // if (!link.includes('%0D%0A')) {
  //     //   link = link.replaceAll('%0A', '%0D%0A')
  //     // }
  //     console.log('Opening mailto link:', link)
  //     window.open(link, '_blank')
  //     onFinished()
  //   /* } else if (service === 'telegram') {
  //     setDialogVisible(true) */
  //     // } else if (service === 'signal') {
  //   } else throw new Error('Invalid service: ' + service)
  // }

  return (
    <>
      <FlexRow className="w-full justify-around gap-4 items-center flex-wrap">
        {/* <div className="border-2 rounded-lg p-4 cursor-pointer"><Signal onClick={() => handleShip('signal')} /></div> */}

        <DiscordButton {...{ moralisPlacedOrderId }}/>
        {/* <ButtonTW id="telegram"
          buttonType="outline"
          className="min-w-max-content text-lg tracking-widest m-0 flex flex-row"
          onClick={() => handleShip('telegram')}>
          <Telegram />&nbsp;Telegram
        </ButtonTW> */}
        {/* <ButtonTW id="email"
          buttonType="outline"
          className="min-w-max-content text-lg tracking-widest m-0 flex flex-row"
          onClick={() => handleShip('email')}>
          <Email/>&nbsp;Email
        </ButtonTW> */}
      </FlexRow>
      <Dialog
        open={dialogVisible}
        onClose={closeDialog}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Copy &amp; Send</DialogTitle>
        <DialogContent className="flex flex-col justify-stretch">
          {/* <DialogContentText id="alert-dialog-description">
            <pre>{JSON.stringify(copyState, undefined, 4)}</pre>
          </DialogContentText> */}
          <Button disabled={!!copyState.value} variant="contained" onClick={() => {
            copyToClipboard(signedMessage)
            enqueueSnackbar('Copied to clipboard', { variant: 'success' })
          }}
          >
            Copy to clipboard
          </Button>
          <div className="mb-2" />
          <Button variant="contained" disabled={!copyState.value} onClick={() => {
            window.open(`tg://resolve?domain=${process.env.REACT_APP_ORDER_TELEGRAM}`)
            setDialogActionDone(true)
          }}
          >
            Open telegram chat
          </Button>
        </DialogContent>
        <DialogActions>
          <Button onClick={closeDialog}>
            Cancel
          </Button>
          <Button color="primary" disabled={!dialogActionDone} onClick={() => {
            closeDialog()
            onFinished()
          }}
          >
            Done
          </Button>
        </DialogActions>
      </Dialog>
    </>
  )
})
export function BuyButton (props) {
  const { setAppState } = useAppContext()

  function handleToggleCheckout (tradeType) {
    setAppState(state => ({ ...state, visible: !state.visible, tradeType }))
  }

  return (
    <ButtonTW
      disabled={false}
      buttonType="primary"
      onClick={() => {
        ackeeBuyHome()
        ackeeBuyFunnel('home')
        handleToggleCheckout(MODAL_TYPES.BUY)
      }}
      {...props}
    >
      Buy
    </ButtonTW>
  )
}

export function SellButton ({ balanceWINE, ...rest }) {
  const { setAppState } = useAppContext()
  // const { account } = useActiveWeb3React()

  function handleToggleCheckout (tradeType) {
    setAppState(state => ({ ...state, visible: !state.visible, tradeType }))
  }

  return (
    <ButtonTW
      disabled={!(balanceWINE > 0)}
      onClick={() => {
        handleToggleCheckout(MODAL_TYPES.SELL)
      }}
      {...rest}
    >
      Sell
    </ButtonTW>

  )
}
export function ClaimButton ({ balanceWINE, ...rest }) {
  const { setAppState } = useAppContext()
  const { account } = useActiveWeb3React()

  function handleToggleCheckout (tradeType) {
    setAppState(state => ({ ...state, visible: !state.visible, tradeType }))
  }

  return (
    <ButtonTW
      disabled={
        account === null
        || !balanceWINE
        || balanceWINE.lt(BigNumber.from(1).pow(BigNumber.from(18)))
      }
      onClick={() => {
        handleToggleCheckout(MODAL_TYPES.CLAIM)
      }}
      {...rest}
    >
      Claim
    </ButtonTW>
  )
}
export function ShipButton ({ balanceUNIWINE, pending, ...rest }) {
  const { setAppState } = useAppContext()
  const { account } = useActiveWeb3React()

  function handleToggleCheckout (tradeType) {
    setAppState(state => ({ ...state, visible: !state.visible, tradeType }))
  }

  return (
    <ButtonTW
      pending={pending}
      disabled={
        account === null
        || !balanceUNIWINE
        || balanceUNIWINE.lt(BigNumber.from(1).pow(BigNumber.from(18)))
      }
      onClick={() => {
        ackeeShipHome()
        handleToggleCheckout(MODAL_TYPES.SHIP)
      }}
      {...rest}
    >
      {pending ? '' : 'Ship'}
    </ButtonTW>
  )
}
