import { useWeb3React } from '@web3-react/core'
import { useEffect, useState } from 'react'
import { useEagerConnect, useInactiveListener } from '../../hooks/web3'
// import { useTranslation } from 'react-i18next'
import { network } from '../../network/connectors'
import { NetworkContextName } from '../../utils/constants'
import Loader from '../Loader'

const MessageWrapper = ({ children, className: passedClasses }: { children?: any, className?: string }) => {
  return (
    <div className={`flex justify-center items-center h-80 ${passedClasses}`}>
      {children}
    </div>
  )
}

export default function Web3ReactManager ({ children }: { children: JSX.Element }) {
  // const { t } = useTranslation()
  const { active } = useWeb3React()
  const { active: networkActive, error: networkError, activate: activateNetwork } = useWeb3React(NetworkContextName)

  // try to eagerly connect to an injected provider, if it exists and has granted access already
  const triedEager = useEagerConnect()

  // after eagerly trying injected, if the network connect ever isn't active or in an error state, activate it
  useEffect(() => {
    if (triedEager && !networkActive && (networkError == null) && !active) {
      console.log('Eager connect failed, connecting to network')
      activateNetwork(network, error => {
        console.error('Network provider error:', error)
        // TODO: snackbar? dialog? in which situations does this happen?
      })
    }
  }, [triedEager, networkActive, networkError, activateNetwork, active])

  // when there's no account connected, react to logins (broadly speaking) on the injected provider, if it exists
  useInactiveListener(!triedEager)

  // handle delayed loader state
  const [showLoader, setShowLoader] = useState(false)
  useEffect(() => {
    const timeout = setTimeout(() => {
      setShowLoader(true)
    }, 600)

    return () => {
      clearTimeout(timeout)
    }
  }, [])

  // on page load, do nothing until we've tried to connect to the injected connector
  if (!triedEager) {
    return null
  }

  // if the account context isn't active, and there's an error on the network context, it's an irrecoverable error
  if (!active && (networkError != null)) {
    return (
      <MessageWrapper>
        Unknown network error { networkError }
      </MessageWrapper>
    )
  }

  // if neither context is active, spin
  if (!active && !networkActive) {
    return showLoader
      ? (
        <MessageWrapper>
          <Loader size="3rem" />
        </MessageWrapper>
        )
      : null
  }

  return children
}
