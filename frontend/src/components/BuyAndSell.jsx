// import mapValues from 'lodash/mapValues'
import mapValues from 'lodash/mapValues'
import { useEffect, useMemo, useState } from 'react'
import { Link } from 'react-router-dom'
import { useAppContext } from '../context'
import { useEthContext } from '../context/eth-data'
import { useActiveWeb3React } from '../hooks/web3'
import question from '../img/question.svg'
import { useUIContext } from '../pages/Body/UIContext'
import { ackeeBuyBuy, ackeeBuyFunnel } from '../utils/ackee'
import { amountFormatter, ERROR_CODES, FT_NAME, INITIAL_SUPPLY, MODAL_TYPES, sleep } from '../utils/main-utils'
import { rIF } from '../utils/react-utils'
import { isWalletCancelError } from '../utils/web3-utils'
import { ConnectButton } from './Buttons'
import { ButtonTW } from './ButtonTW'
import NFTimage from './Gallery/wineLandscapeStillLife.jpg'
import IncrementToken from './IncrementToken'
import ModalContent from './ModalContent'
import { ProcessTracker } from './ProcessTracker'
import SelectToken from './SelectToken'

const MAX_BUY_LIMIT = 3

export function useCount () {
  const { appState, setAppState } = useAppContext()

  function increment () {
    setAppState(state => ({ ...state, count: state.count + 1 }))
  }

  function decrement () {
    if (appState.count >= 1) {
      setAppState(state => ({ ...state, count: state.count - 1 }))
    }
  }

  function setCount (val) {
    const int = val.toInt()
    setAppState(state => ({ ...state, count: int }))
  }
  return [appState.count, increment, decrement, setCount]
}

function getValidationErrorMessage (validationError) {
  if (!validationError) {
    return null
  } else {
    switch (validationError.code) {
      case ERROR_CODES.INVALID_AMOUNT: {
        return 'Invalid Amount'
      }
      case ERROR_CODES.INVALID_TRADE: {
        return 'Invalid Trade'
      }
      case ERROR_CODES.INSUFFICIENT_ALLOWANCE: {
        return 'Set Allowance to Continue'
      }
      case ERROR_CODES.INSUFFICIENT_ETH_GAS: {
        return 'Not Enough ETH to Pay Gas'
      }
      case ERROR_CODES.INSUFFICIENT_SELECTED_TOKEN_BALANCE: {
        return 'Not Enough of Selected Token'
      }
      default: {
        return validationError.code
      }
    }
  }
}

export default function BuyAndSell ({ setShowConnect }) {
  const { appState } = useAppContext()
  const { errorDialog } = useUIContext()
  const { account } = useActiveWeb3React()
  let {
    ethData: {
      selectedTokenSymbol,
      setSelectedTokenSymbol,
      ready,
      validateBuy,
      validateSell,
      reserveWineTokens,
      // totalSupply,
      performTrade,
    // dollarize,
    },
    process,
  } = useEthContext() /* as EthData */

  if (appState.fake) {
    performTrade = async function (trade, slippageBounds, process) {
      await process.trackStep({ type: 'approve', status: 'wait', hash: '0xaebb59ed8887010ebc6e35e8d7fe1049391f2ae2dad8ce6bd899385049773e31' },
        () => sleep(1000))
      await process.trackStep({ type: 'trade', status: 'wait', hash: '0x50d8a98ec306d01af5233eca7fcba797ed514cf26dee7e9c03b98f83ab34be7d' },
        () => sleep(1000))
      process.setSuccessful()
      return { hash: '0x50d8a98ec306d01af5233eca7fcba797ed514cf26dee7e9c03b98f83ab34be7d' }
    }
  }

  const buying = appState.tradeType === MODAL_TYPES.BUY
  const selling = !buying
  const [buyValidationState, setBuyValidationState] = useState({})
  const [sellValidationState, setSellValidationState] = useState({})
  const [validationError, setValidationError] = useState()
  const validationState = useMemo(() => buying ? buyValidationState : sellValidationState, [buyValidationState, buying, sellValidationState])
  const errorMessage = getValidationErrorMessage(validationError)
  // console.debug('State trade type', state.tradeType, { ready, pending, buyValidationState })

  // Use effect to start process, as we need to wait for react state reset
  useEffect(() => {
    if (process.active && !process.steps.length) {
      console.log('Starting process')
      performTrade(
        validationState.trade,
        validationState.slippageBounds,
        process,
      ).catch(error => {
        if (!isWalletCancelError(error)) {
          errorDialog(`Failed to ${buying ? 'buy' : 'sell'}`, error)
        }
      })
    }
  }, [performTrade, appState.process, appState.count, appState.fake, buying, validationState, process, errorDialog])

  function getText (account, buying, errorMessage, ready, process) {
    if (account === null) {
      return 'Connect Wallet'
    } else if (ready && !errorMessage) {
      if (!buying) {
        if (process.active) {
          return '...'
        } else {
          return /* (shouldRenderUnlock ? 'Unlock & ' : '') + */ 'Sell'
        }
      } else {
        if (process.active) {
          return '...'
        } else {
          return /* (shouldRenderUnlock ? 'Unlock & ' : '') + */ 'Buy'
        }
      }
    } else {
      return errorMessage ? 'Error' : 'Loading...'
    }
  }

  // buy state validation
  useEffect(() => {
    if (ready && buying) {
      try {
        const { error: validationError, ...validationState } = validateBuy(String(appState.count))
        // console.debug("buy validation result", validationError.code, mapValues(validationState, bn => bn.toString()))
        setBuyValidationState(validationState)
        setValidationError(validationError || null)

        return () => {
          setBuyValidationState({})
          setValidationError()
        }
      } catch (error) {
        console.log('buyValidation error', error)
        setBuyValidationState({})
        setValidationError(error)
      }
    }
  }, [ready, buying, validateBuy, appState.count])

  // sell state validation
  useEffect(() => {
    if (ready && selling) {
      try {
        const { error: validationError, ...validationState } = validateSell(String(appState.count))
        console.debug('sell validation result', validationError?.code, mapValues(validationState, bn => bn.toString()))
        setSellValidationState(validationState)
        setValidationError(validationError || null)

        return () => {
          setSellValidationState({})
          setValidationError()
        }
      } catch (error) {
        setSellValidationState({})
        setValidationError(error)
      }
    }
  }, [ready, selling, validateSell, appState.count])

  function TokenVal () {
    if (buying && buyValidationState.inputAmount) {
      return amountFormatter(buyValidationState.inputAmount, 18, 4)
    } else if (selling && sellValidationState.outputAmount) {
      return amountFormatter(sellValidationState.outputAmount, 18, 4)
    } else {
      return '0'
    }
  }

  const shouldRenderUnlock = validationState.allowanceNeeded
  const wineTokensRemaining = amountFormatter(reserveWineTokens, 18, 0)

  // const bottlesClaimed = 17 // TODO datify
  // const bottlesPerTerroir = 6
  // const numberOfTerroir = bottlesPerTerroir / totalSupply
  // const currentTerroir = Math.floor(bottlesClaimed / bottlesPerTerroir)
  // const terroir = (totalSupply - bottlesClaimed) % bottlesPerTerroir
  // const terroirInfo = `${terroir}/6 for current terroir (#${currentTerroir})`
  const maxTokens = INITIAL_SUPPLY

  return (
    <ModalContent
      img={NFTimage}
      renderInfo={() => <>
        <p>{(buying && reserveWineTokens
          ? <>
            {`${wineTokensRemaining} out of ${maxTokens} tokens available`}
            {rIF(!process.active, <Link className="inline-block ml-4 -mb-0.5" to="/faq#Availability">
              <img className="h-4" src={question} alt="help / info button" />
            </Link>)}
          </>
          : !process.active
              ? (selling ? 'Are you selling the dip? Buy more instead?' : 'Loading stats...')
              : `Selling ${FT_NAME} for ${selectedTokenSymbol}`)
          }</p>
        {rIF(shouldRenderUnlock && !process.steps.length, <p>You need to allow Uniswap to spend your {buying ? setSelectedTokenSymbol : 'WINE'}.</p>)}
      </>}
    >
      {rIF(account !== null && validationError, <span className="text-red-500 text-bold">{errorMessage}</span>)}

      <ProcessTracker process={process} />

      {rIF(!process.active,
        <div className="w-full flex flex-row flex-wrap items-center justify-around m-0">
          {rIF(account !== null,
            <>
              <IncrementToken className="my-3" max={buying ? MAX_BUY_LIMIT : null} />
              <SelectToken
                className="text-gray-800 max-w-2/5 text-xl my-3 mx-2"
                selectedTokenSymbol={selectedTokenSymbol}
                setSelectedTokenSymbol={setSelectedTokenSymbol}
                prefix={TokenVal()}
              />
            </>,
          )}
          {account
            ? <ButtonTW
                pending={process.active}
                disabled={!account || validationError !== null || process.active}
                className={'my-3 sm:w-auto' + (validationError ? ' text-red' : '')}
                onClick={() => {
                  ackeeBuyBuy()
                  ackeeBuyFunnel('buy')
                  process.start()
                }}
            >
              {getText(account, buying, errorMessage, ready, process)}
            </ButtonTW>
            : <ConnectButton buttonText='Connect Wallet' {...{ setShowConnect }} />
          }
        </div>,
      )}
    </ModalContent>
  )
}
