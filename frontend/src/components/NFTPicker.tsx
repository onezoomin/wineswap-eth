import { ChevronLeft, ChevronRight, LocalShippingOutlined } from '@material-ui/icons'
import { Token } from '@uniswap/sdk-core'
import filter from 'lodash/filter'
import find from 'lodash/find'
import union from 'lodash/union'
import without from 'lodash/without'
import { Dispatch, FunctionComponent, HTMLAttributes, ReactNode, SetStateAction, useEffect, useState } from 'react'
import Image from 'react-graceful-image'
import { useSwipeable } from 'react-swipeable'
import opensea from '../img/opensea-logo.webp'
import { WineToken, WineTokenMeta } from '../utils/main-utils'
import { appendClassNames, rIF } from '../utils/react-utils'
import Loader from './Loader'
import { OpenseaLink } from './OpenseaLink'

export const NFTPicker: FunctionComponent<{
  tokens?: WineToken[]
  tokenType?: Token|null
  highlighted?: number[]
  tokenMeta: {[key: string]: WineTokenMeta | null}
  selected: number[] | null
  setSelected?: Dispatch<SetStateAction<number[]>>
  className?: string }
> = ({ tokens, tokenMeta, tokenType, selected, setSelected, className = '', highlighted }) => {
  //
  const [centeredOn, setCenteredOn] = useState(0)
  // Remove tokens that are not in the original list anymore
  useEffect(() => {
    if (!setSelected) return
    const obsolete = filter(selected, id => !find(tokens, { id }))
    if (obsolete.length) {
      console.warn('NFTPicker removing obsolete from selected list:', obsolete)
      setSelected(s => without(s, obsolete))
    }
  }, [tokens, selected, setSelected])
  const renderDot = (token: WineToken, idx: number): JSX.Element => {
    // const dotClasses = `nav-dot relative m-2 bg-gray-50 rounded-full ${idx === centeredOn ? 'w-2 h-2' : 'w-1.5 h-1.5'}`
    const w = tokens && tokens.length > 8 ? 3 : 6 // hack to prevent purgecss: w-3 w-6
    const lineClasses = `nav-dot mx-1 h-0.5 w-${w} bg-gray-50 rounded-full relative cursor-pointer ${idx === centeredOn ? '' : 'opacity-30'}`
    return (
      <div
        className={lineClasses}
        onClick={() => setCenteredOn(idx)}
        key={idx}
      ></div>
    )
  }
  const tokensLength = tokens?.length ?? 0
  const prev = () => { setCenteredOn(Math.max(0, centeredOn - 1)) }
  const next = () => { setCenteredOn(Math.min(tokensLength - 1, centeredOn + 1)) }

  const NavDots = (): JSX.Element | null => {
    if (!tokensLength) return null

    return (
      <div className="flex flex-row items-stretch justify-around w-full h-max-content p-4 overflow-hidden" >
        {tokens && tokens.length < 2 ? null : <ChevronLeft onClick={prev} />}
        <div className="flex-none flex flex-nowrap items-center">
          {!tokens?.length ? null : tokens.map(renderDot)}
        </div>
        {tokens && tokens.length < 2 ? null : <ChevronRight onClick={next} />}
      </div>
    )
  }
  const renderSingle = (token: WineToken, idx: number): JSX.Element => {
    let classes = `${idx === centeredOn ? 'w-48 h-48' : 'w-40 h-40'} cursor-pointer m-2`
    const isSelected = selected?.includes(token.id)
    classes += isSelected ? ' ring-4 ring-gray-50' : ''
    // console.debug(token.id, isSelected, selected, url)
    const isHightlighted = highlighted?.includes(token.id)
    classes += isHightlighted ? ' ring-4 ring-gray-50' : ''
    return (
      <NFTSingle
        key={token.id}
        token={token}
        meta={tokenMeta[token.id]}
        className={classes}
        isHighlighted={isHightlighted}
        renderBadge={rIF(idx === centeredOn && tokenType,
          <OpenseaLink token={tokenType as Token} tokenID={token.id} onClick={e => { console.log('Opensea click, e'); e.stopPropagation() }}>
            <img src={opensea} className="h-10 w-10 absolute bottom-2 right-2" />
          </OpenseaLink>,
        )}
        onClick={() => {
          if (!setSelected) return
          setCenteredOn(idx)
          if (selected === null) return // selection functionality not active
          if (!isSelected) setSelected(selected => union(selected, [token.id]))
          else setSelected(selected => without(selected, token.id))
        }}
      />
    )
  }

  const swipeHandlers = useSwipeable({
    // onSwiped: (eventData) => console.log("User Swiped!", eventData),
    onSwipedRight: prev,
    onSwipedLeft: next,
    delta: 10, // min distance(px) before a swipe starts
    preventDefaultTouchmoveEvent: false, // call e.preventDefault *See Details*
    trackTouch: true, // track touch input
    trackMouse: true, // track mouse input
    rotationAngle: 0, // set a rotation angle
  })
  const applySwipeHandlers = !tokens?.length ? {} : swipeHandlers

  return (
    <div className={className + 'my-2 w-full flex flex-col items-start'}>
      <div
        {...applySwipeHandlers}
        className="flex flex-row items-center transition-all duration-700 delay-150"
        style={{ minHeight: '208px', paddingLeft: 'calc(50% - 6rem - 0.5rem)' /* center - NFT width - NFT margin */, marginLeft: `-${11 * centeredOn}rem` }}
      >
        {!tokens
          ? <p>Loading tokens...</p>
          : (!tokens.length
              ? <p>No tokens {':('}</p>
              : tokens.map(renderSingle)
            )
        }
      </div>
      <NavDots />
    </div>
  )
}
export const NFTSingle: FunctionComponent<HTMLAttributes<HTMLDivElement> &{
  token: WineToken
  meta: WineTokenMeta|null
  isHighlighted?: boolean
  renderBadge?: ReactNode
}> = ({ token, meta, isHighlighted, renderBadge, className: passedClasses, ...props }) => {
  const defaultClasses = 'transition-all duration-300 rounded-md overflow-hidden relative'
  const mergedClasses = appendClassNames(defaultClasses, passedClasses)

  const placeholder = () => <div className="flex w-full h-full items-center justify-center p-4" style={{ backgroundColor: '#380015' }}>
    <Loader size="69px" strokeWidth="0.5" />
  </div>

  return <div {...mergedClasses} /* style={{ minHeight: '10rem' }} */ {...props}>
    {!meta?.image
      ? placeholder()
      : <Image
          className="w-full h-full object-cover"
        /* placeholderColor="#722F37" */
          customPlaceholder={placeholder}
          alt={`NFT ${token?.id ?? '?'}`}
          src={meta?.image}
          noLazyLoad={true}
      />
    }
    <div className="absolute top-0 z-1 w-full h-full flex flex-col items-center justify-center text-xl transition-opacity opacity-0 hover:opacity-100 bg-black bg-opacity-40">
      NFT #{token?.id ?? '?'}
      {isHighlighted ? <LocalShippingOutlined /> : null}
    </div>
    {renderBadge}
  </div>
}
