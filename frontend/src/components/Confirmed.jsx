import { useEffect } from 'react'
import { useAppContext } from '../context'
import { useEthContext } from '../context/eth-data'
import { useDetectNetwork } from '../hooks/main'
import { useActiveWeb3React } from '../hooks/web3'
import { useUIContext } from '../pages/Body/UIContext'
import { FT_NAME, MODAL_TYPES, requestWalletTokenAdd } from '../utils/main-utils'
import { ButtonTW } from './ButtonTW'
import { EtherscanLink } from './EtherscanLink'
import NFTimage from './Gallery/wineLandscapeStillLife.jpg'
import { ModalBack } from './ModalBack'
import ModalContent from './ModalContent'
import { ProcessTracker } from './ProcessTracker'
// import close from './Gallery/close.svg'

export default function Confirmed ({
  ethData,
  type,
  hash,
  amount,
  clearLastTransaction,
}) {
  const { WINE } = ethData
  const { library } = useActiveWeb3React()
  const { appState, updateAppState } = useAppContext()
  const { process } = useEthContext()
  const { networkName } = useDetectNetwork()
  const { errorDialog } = useUIContext()

  useEffect(() => {
    if (!appState.visible) {
      clearLastTransaction()
    }
  }, [appState.visible, clearLastTransaction])

  if (type === MODAL_TYPES.BUY) {
    return (<ModalContent img={NFTimage}>

      <div className="mb-4">
        <p className="text-md text-center">You successfully bought {amount} {FT_NAME} FT!</p>
      </div>

      <ProcessTracker process={process} />

      <ButtonTW
        buttonType="outline"
        className="max-w-fit mb-2"
        onClickWithLoad={() => requestWalletTokenAdd(library, WINE/* TODO: image URL */)
          .catch(error => errorDialog('Failed to add token to wallet', error, <p>
            You could register it manually:<br />
            <code>{WINE.address}</code>
          </p>))}
      >
        Add to Wallet
      </ButtonTW>

      <ButtonTW
        className="max-w-fit mb-2"
        onClick={() => {
          clearLastTransaction()
          updateAppState({ tradeType: MODAL_TYPES.CLAIM, count: 1 })
        }}
      >
        Claim now
      </ButtonTW>

      <ModalBack className="pb-0" onClick={() => {
        updateAppState({ count: 1 })
        process.reset()
      }}
      />
    </ModalContent>)
  } else {
    return (<ModalContent img={NFTimage}>
      <div className="mb-4">
        <p className="text-md text-center">You successfully sold {amount} {FT_NAME} FT!</p>
      </div>
      <div className="text-sm mb-4">
        <EtherscanLink trx={hash} networkName={networkName} />
      </div>

      <ModalBack className="pb-0" onClick={() => {
        updateAppState({ count: 1 })
        process.reset()
      }}
      />
    </ModalContent>
    )
  }
}
