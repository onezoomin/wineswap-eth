import { Web3Provider } from '@ethersproject/providers'
import { nullish } from './constants'
import { formatError } from './error'

declare global {
  interface Window {
    ethereum?: Web3Provider
  }
}

export function isWalletCancelError (error: Error|object|string|nullish) {
  const msg = error instanceof Error ? (formatError(error)) : error
  if (typeof msg === 'string' && msg.includes('User denied')) return true
  if (typeof msg === 'string' && msg.includes('cancelled')) return true
  return false
}
