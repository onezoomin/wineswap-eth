import * as ackeeTrackerLib from 'ackee-tracker'

export const ackeeTracker = ackeeTrackerLib.create('https://wineswap-ackee.vercel.app',
  {
    ignoreLocalhost: false,
    ignoreOwnVisits: false,
  })

export const ackeeBuyHome = () => ackeeTracker.action('0dbd7522-c887-49d0-86d4-5fc16ceb5bad', { key: 'Click', value: 1 })
export const ackeeBuyBuy = () => ackeeTracker.action('613e3813-bce3-4cf9-be2b-78fee4af8b22', { key: 'Click', value: 1 })
export const ackeeBuySuccess = () => ackeeTracker.action('55bea714-1335-477a-8f05-e9cf70e11b1e', { key: 'Click', value: 1 })
export const ackeeBuyFunnel = (key: string) => ackeeTracker.action('75b0db79-bcea-4839-9a34-cd36bffd0d6b', { key, value: 1 })

export const ackeeClaimFunnel = (key: string) => ackeeTracker.action('4221dfe0-0cf5-431d-a190-601959800447', { key, value: 1 })

export const ackeeShipHome = () => ackeeTracker.action('da75f988-713a-4fc5-ad14-240c82d598e7', { key: 'Click', value: 1 })
export const ackeeShipShip = () => ackeeTracker.action('5e72a720-65bf-4af7-8e5e-ecc131ed976b', { key: 'Click', value: 1 })
