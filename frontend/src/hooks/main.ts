import { isAddress as isValidAddress } from '@ethersproject/address'
import { Web3Provider } from '@ethersproject/providers'
import { Token, TokenAmount } from '@uniswap/sdk-core'
import IUniswapV2Pair from '@uniswap/v2-core/build/IUniswapV2Pair.json'
import IUniswapV2Router02 from '@uniswap/v2-periphery/build/IUniswapV2Router02.json'
import { Pair } from '@uniswap/v2-sdk'
import { BigNumber, Contract } from 'ethers'
import { useCallback, useContext, useEffect, useMemo, useState } from 'react'
import { EthBlockContext } from '../components/Web3ReactManager/block-listener'
import { nullish } from '../utils/constants'
import {
  AddressMap,
  ERC20_ABI,
  getContract,
  getEtherBalance,
  getTokenAllowance,
  getTokenBalance,
  getWineToken,
  getWineTokenList,
  ROUTER_ADDRESS,
  TOKEN_ADDRESSES,
  UNIWINE_ABI,
  WineToken,
  WineTokenMeta,
} from '../utils/main-utils'
import { useActiveWeb3React } from './web3'

export function isAddress (address: string|nullish) {
  if (!address) return false
  return isValidAddress(address)
}

export function useDetectNetwork () {
  const { library } = useActiveWeb3React()
  const [error, setError] = useState<Error | null>()
  const [id, setID] = useState<number | null>()
  const [name, setName] = useState<string | null>(null)
  const [addresses, setAddresses] = useState<AddressMap | null>(null)
  const routerContract = useContract(name ? ROUTER_ADDRESS[name] : null, IUniswapV2Router02.abi)

  useEffect(() => {
    let stale = false;
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    (async () => {
      try {
        setError(null)
        const net = (await library?.getNetwork())
        if (net == null) return
        if (stale) return
        console.debug('Detected network:', net)
        setID(net.chainId)
        setName(net.name)
        setAddresses(TOKEN_ADDRESSES[net.name])
      } catch (error) {
        if (stale) return
        console.error('Detect network error:', error)
        setError(error)
      }
    })()

    return () => {
      stale = true
    }
  }, [library])

  return { networkID: id, networkName: name, addresses, routerContract, networkError: error }
}

export function useContract (tokenAddress: string|undefined, abi: unknown, withSignerIfPossible = true) {
  const { library, account } = useActiveWeb3React()

  return useMemo(() => {
    try {
      return library && tokenAddress
        ? getContract(tokenAddress, abi, library, withSignerIfPossible && account ? account : undefined)
        : null
    } catch (error) {
      console.error('failed to get contract', error, { tokenAddress, abi, withSignerIfPossible })
      return null
    }
  }, [abi, account, library, tokenAddress, withSignerIfPossible])
}

export function useTokenContract (tokenAddress?: string, withSignerIfPossible = true) {
  return useContract(tokenAddress, ERC20_ABI, withSignerIfPossible)
}

/**
 * Wrap a function that will fetch new data for each Block automatically
 *
 * @param func Will be called for each block to fetch the latest data
 * @param setValue Will be called with the final value (or null on error)
 * @param onError Will be called to handle / log error (setValue doesn't need to be called)
 */
export function useFunctionWithBlockMemo<R> (func: () => Promise<R|null>, setValue: (value: R|null) => any, onError: (err: Error) => any) {
  const block = useContext(EthBlockContext)

  useEffect(() => {
    let stale = false

    func().then(
      value => {
        if (!stale) {
          // console.log('useFunctionWithBlockMemo result:', value, func)
          setValue(value)
        }
      },
      err => {
        if (!stale) {
          setValue(null)
          onError(err)
        }
      })
    return () => {
      stale = true
    }
  }, [block, func, setValue, onError])
}

export function useExchangePair (tokenA?: Token|null, tokenB?: Token|null): [Pair|null, Contract|null] {
  const web3 = useActiveWeb3React()
  const [pair, setPair] = useState<Pair | null>(null)
  const [contract, setContract] = useState<Contract | null>(null)

  useFunctionWithBlockMemo<{pair: Pair|null, contract: Contract|null}>(
    useCallback(async () => {
      if (web3?.library == null || (tokenA == null) || tokenB == null) return null

      const [token0, token1] = tokenA.sortsBefore(tokenB) ? [tokenA, tokenB] : [tokenB, tokenA]
      const pairAddress = Pair.getAddress(token0, token1)
      // console.debug('Pair for', tokenA.name, tokenB.name, '->', pairAddress)
      const contract = getContract(pairAddress, IUniswapV2Pair.abi, web3.library)

      const reserves = await contract.getReserves()
      // console.log("Received reserves:", reserves)
      const [reserve0, reserve1] = reserves

      const pair = new Pair(new TokenAmount(token0, reserve0), new TokenAmount(token1, reserve1))
      return { pair, contract }
    }, [web3, tokenA, tokenB]),
    useCallback(result => {
      const { pair, contract } = result ?? { pair: null, contract: null } // can't destructure from null, so we create an object to destructure from
      setPair(pair)
      setContract(contract)
    }, [setPair, setContract]),
    useCallback(err => console.error('[useExchangePair]', { tokenA, tokenB }, err), [tokenA, tokenB]),
  )

  return [pair, contract]
}

export function useAddressBalance (address: any, tokenAddress?: string) {
  const { library } = useActiveWeb3React()
  const [balance, setBalance] = useState<BigNumber | null>(null)

  useFunctionWithBlockMemo<BigNumber>(
    useCallback(async () => {
      // console.log('useAddressBalance:', !!library, address, tokenAddress)
      if (!isAddress(address) || !tokenAddress || (tokenAddress !== 'ETH' && !isAddress(tokenAddress))) return null

      return (tokenAddress === 'ETH'
        ? await getEtherBalance(address, library)
        : await getTokenBalance(tokenAddress, address, library))
    }, [address, tokenAddress, library]),
    setBalance,
    useCallback(err => console.error('[addressBalance]', tokenAddress, err), [tokenAddress]),
  )
  return balance
}

export function useUniwineTokenList (address: any, tokenAddress?: string): WineToken[]|null {
  const { library } = useActiveWeb3React()
  const [tokens, setTokens] = useState<WineToken[] | null>(null)

  useFunctionWithBlockMemo<WineToken[]>(
    useCallback(async () => {
      if (!isAddress(address) || !tokenAddress || (tokenAddress !== 'ETH' && !isAddress(tokenAddress))) return null

      return await getWineTokenList(tokenAddress, address, library) // TODO: fill loaded tokens already to make UI snappier?
    }, [address, tokenAddress, library]),
    setTokens, // TODO: only set in case of changes: useCallback(newTokens => setTokens((oldTokens) => ...), []),
    useCallback(err => console.error('[useWineTokenList]', tokenAddress, address, err), [address, tokenAddress]),
  )
  return tokens
}

export function useNextUniwineToken (library: Web3Provider|undefined, uniwineAddress?: string|null, totalSupply?: BigNumber|null): WineToken | null {
  const [token, setToken] = useState<WineToken | null>(null)

  useFunctionWithBlockMemo<WineToken>(
    useCallback(async () => {
      if (!library || !totalSupply || !uniwineAddress) return null
      const Contract = getContract(uniwineAddress, UNIWINE_ABI, library)
      return await getWineToken(Contract, totalSupply.toNumber())
    }, [library, totalSupply, uniwineAddress]),
    setToken, // TODO: only set in case of changes: useCallback(newTokens => setTokens((oldTokens) => ...), []),
    useCallback(err => console.error('[useNextUniwineToken]', uniwineAddress, totalSupply, err), [uniwineAddress, totalSupply]),
  )
  return token
}

const metaCache: {[key: string]: WineTokenMeta} = {}
export function useUniwineMeta (tokens: WineToken[]): {[key: string]: WineTokenMeta|null} {
  const { library } = useActiveWeb3React()
  const [metaMap, setMetaMap] = useState<{[key: string]: WineTokenMeta|null}>({})
  // const [metaCache, setMetaCache] = useLocalStorage<>('metaCache', {})

  useEffect(() => {
    if (!library || !tokens) return
    let stale = false

    const missing = tokens.filter(t => !metaMap[t.id])
    // console.debug('[Meta] missing?', missing, { tokens, meta: metaMap })
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    missing.forEach(async token => { // runs fetches in parallel
      try {
        let meta = metaCache?.[token.metaURL] // it's IPFS urls, it's (reasonably) safe to assume they return permanent results :P
        if (!meta) {
          // fetch
          console.debug('fetching:', token.metaURL)
          const fetchResult = await fetch(token.metaURL)
          meta = await fetchResult.json() as WineTokenMeta
          console.debug('fetchResult JSON', meta)

          // save to cache
          metaCache[token.metaURL] = meta
          // if (!metaCache) console.warn('metaCache is', metaCache)
          // else setMetaCache(cache => ({ ...cache, [token.metaURL]: meta as WineTokenMeta }))
        }
        if (stale) return
        setMetaMap(oldMetaMap => ({ ...oldMetaMap, [token.id]: meta }))
      } catch (err) {
        console.error('Meta fetch error', err)
      }
    })
    return () => { stale = true }
  }, [library, metaMap, tokens])

  return metaMap
}

export function useTotalSupply (contract) {
  const [totalSupply, setTotalSupply] = useState<BigNumber|null>(null)

  useFunctionWithBlockMemo<BigNumber>(
    useCallback(async () => {
      if (contract == null) return
      return contract.totalSupply()
    }, [contract]),
    setTotalSupply,
    useCallback(err => console.error('[useTotalSupply]', contract, err), [contract]),
  )
  return totalSupply
}

export function useAddressAllowance (account: string|nullish, tokenAddress: Token|string|nullish, spenderAddress: Token|string|nullish) {
  const tokenAddr = tokenAddress instanceof Token ? tokenAddress.address : tokenAddress
  const spenderAddr = spenderAddress instanceof Token ? spenderAddress.address : spenderAddress

  const { library } = useActiveWeb3React()
  const [allowance, setAllowance] = useState<BigNumber|null>()

  useFunctionWithBlockMemo<BigNumber>(
    useCallback(async () => {
      if (!isAddress(account) || !isAddress(tokenAddr) || !isAddress(spenderAddr)) return
      return await getTokenAllowance(account, tokenAddr, spenderAddr, library)
    }, [account, tokenAddr, spenderAddr, library]),
    setAllowance,
    useCallback(err => console.error('[useAddressAllowance]', err, { account, tokenAddr, spenderAddr }), [account, spenderAddr, tokenAddr]),
  )
  return allowance
}
