import debounce from 'lodash/debounce'
import { useCallback, useEffect, useMemo, useState } from 'react'
import useResizeObserver from 'use-resize-observer'
import { hasScrollOverflowY } from './../utils/react-utils'

const resizeListener = () => {
  const vh = window.innerHeight * 0.01
  document.documentElement.style.setProperty('--vh', `${vh}px`)
}
export function useResizeFix () {
  // https://www.markusantonwolf.com/en/articles/solution-to-the-mobile-viewport-height-issue-with-tailwind-css

  useEffect(() => {
    resizeListener()
    window.addEventListener('resize', resizeListener)
    return () => window.removeEventListener('resize', resizeListener)
  })
}

export function useOverflowCheck (additionalDeps = []) {
  const [hasScrollOverflow, setHasScrollOverflow] = useState(false)
  const [node, setRef] = useState<HTMLElement>() // we need to use callback ref to catch updates - https://reactjs.org/docs/hooks-faq.html#how-can-i-measure-a-dom-node

  // Observe if the ModalContent is overflowing //
  const overflowCheck = useCallback(() => {
    if (!node) return
    const overflowNow = hasScrollOverflowY(node)
    console.debug('resizeObserver overflowNow', overflowNow)
    setHasScrollOverflow(overflowNow)
  }, [node])
  const debouncedUpdater = useMemo(() => debounce(overflowCheck, 500), [overflowCheck])
  useResizeObserver({
    ref: node,
    onResize: debouncedUpdater,
  })

  // call once when ref is ready or
  useEffect(() => {
    overflowCheck()
    // debouncedUpdater()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [overflowCheck, node, ...additionalDeps])

  return { hasScrollOverflow, ref: setRef }
}
