import { join } from 'lodash-es'
const DISCORD_SITUATIONS = 'https://discord.com/api/webhooks/850807981435781180/ZTu6SPsq4yOy3TjrVonPi_dqrT2r36MfEr61TV4oHo6Z_jdcrsBKuw8nOs6qhWczXNCY'

export function formatAddressShort (addr) {
  return `0x${addr.slice(2, 5).toUpperCase()}...${addr.slice(-3).toUpperCase()}`
}

export const log = async (LV, ...msgs) => {
  const logger = Moralis.Cloud.getLogger()
  let alsoDiscord
  if (typeof LV === 'number') {
    if (LV === 9) {
      alsoDiscord = true
    }
  } else {
    msgs = msgs ?? []
    msgs.unshift(LV)
  }

  const msgString = join( // .join('\n') - breaks moralis...
    msgs.map(msg => (typeof msg === 'string' ? msg : JSON.stringify(msg, null, 2))),
    '\n ',
  )

  logger.info(msgString)
  if (alsoDiscord) {
    try {
      await Moralis.Cloud.httpRequest({
        method: 'POST',
        url: DISCORD_SITUATIONS,
        body: {
          content: msgString,
        },
      })
    } catch (error) {
      logger.info('caught while logging')
      logger.info(error.data)
    }
  }
}

export async function http ({
  url,
  method = 'POST',
  body = undefined as any|undefined,
  contentType = 'application/json' as string|undefined,
  auth = undefined as string|undefined,
}) {
  const opts: any = {
    method,
    url,
    followRedirects: true,
    headers: {
      Authorization: auth,
    },
  }
  if (auth) opts.headers.Authorization = auth
  if (method !== 'GET' && contentType) opts.headers['Content-Type'] = contentType
  if (body) opts.body = body
  return Moralis.Cloud.httpRequest(opts)
}
