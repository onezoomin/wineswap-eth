function testVerifyLearning () {
  const { verifyMessage } = require('@ethersproject/wallet')
  // before @tennox found the above import technique many steps were needed to bufferize and hash this thing:
  const { fromRpcSig, ecrecover, pubToAddress, toBuffer, hashPersonalMessage, bufferToHex, fromAscii } = require('ethereumjs-util')

  const signedMsg = 'PLEASE VERIFY YOUR ADDRESS (physical and wallet)!\nYour data is only sent to us and will never be shared publicly.\n\nJoshua Gottdenker\n609 5th Ave\n07719 Belmar New Jersey\nUnited States\nme@begreen.nu\nETH Wallet: 0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e\nTimestamp: 2021-06-18T11:33:10.369Z\nNFT IDs: 14'
  const rpcSig = '0x072bd37fe5967a7f6d8930c470ea9a5a1dd48afb5816d388def7ad3c503d0f04241e7f9a3d1234fb12fa7c1dc9e0eadd5c3e9f16473b5607c83a63aae8f2fc1e1c'
  const expectedAddress = ('0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e').toLowerCase()

  const signedMsgBuf = toBuffer(fromAscii(signedMsg))
  const { v, r, s } = fromRpcSig(rpcSig)
  const pubKeyBuffer = ecrecover(hashPersonalMessage(signedMsgBuf), v, r, s)
  const pubAddBuffer = pubToAddress(pubKeyBuffer)
  const pubAddHex = bufferToHex(pubAddBuffer)

  const recoveredAddress = verifyMessage(signedMsg, rpcSig).toLowerCase() // 5 lines into 1
  console.log(expectedAddress, '?=?', bufferToHex(pubAddHex), '?=?', recoveredAddress)

  const isMatching = !!(pubAddHex === expectedAddress)
  return [{ json: { isMatching, expectedAddress, pubAddHex } }]
}

function assertSigner (items) {
  const { body } = items[0].json
  let { account: expectedAddress, signedMessage, signature } = body
  expectedAddress = expectedAddress.toLowerCase()

  const { verifyMessage } = require('@ethersproject/wallet') // https://docs.ethers.io/v5/single-page/#/v5/api/utils/signing-key/-%23-utils-verifyMessage

  const recoveredAddress = verifyMessage(signedMessage, signature).toLowerCase()
  const isSignerMatching = !!(recoveredAddress === expectedAddress.toLowerCase())

  if (!isSignerMatching) {
    function findDiff (str1, str2) {
      let diff = ''
      str2.split('').forEach(function (val, i) {
        if (val !== str1.charAt(i)) {
          diff += val
        } else {
          diff += '='
        }
      })
      return diff
    }
    function toB64 (data) {
      const buff = Buffer.from(data)
      return buff.toString('base64')
    }
    const workingSignedMessage = 'PLEASE VERIFY YOUR ADDRESS (physical and wallet)!\nYour data is only sent to us and will never be shared publicly.\n\nJoshua Gottdenker\n609 5th Ave\n07719 Belmar New Jersey\nUnited States\nme@begreen.nu\nETH Wallet: 0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e\nTimestamp: 2021-06-18T11:33:10.369Z\nNFT IDs: 14'
    const msgDiff = findDiff(workingSignedMessage, signedMessage)
    const b64Working = toB64(workingSignedMessage)
    const b64Broken = toB64(signedMessage)
    const b64Diff = findDiff(b64Working, b64Broken)

    const workingSignature = '0x072bd37fe5967a7f6d8930c470ea9a5a1dd48afb5816d388def7ad3c503d0f04241e7f9a3d1234fb12fa7c1dc9e0eadd5c3e9f16473b5607c83a63aae8f2fc1e1c'
    // const expectedAddress = ('0xAfaC41C116F7bdB2C46b142Ca1c74520a112456e').toLowerCase()
    const isMessageMatching = !!(workingSignedMessage === signedMessage)
    const isSigMatching = !!(workingSignature === signature)
    return [{ json: { body, isSignerMatching, expectedAddress, recoveredAddress, msgDiff, b64Working, b64Broken, b64Diff, isSigMatching, isMessageMatching } }]
  }
  return [{ json: { body, isSignerMatching, expectedAddress, recoveredAddress } }]
}
