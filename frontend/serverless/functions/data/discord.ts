import { log } from './../utils'
export const DiscordUser = Moralis.Object.extend('DiscordUser')

export async function createOrUpdateDiscordUser (authData, userInfo, ethAddress, moralisPlacedOrderId) {
  let user = await (new Moralis.Query(DiscordUser)
    .equalTo('discordID', userInfo.id)
    .first())
  if (!user) {
    // log('[createOrUpdateDiscordUser] user did not exist:', userInfo.id)
    user = new DiscordUser()
    user.set('discordID', userInfo.id)
  } else {
    //  log('[createOrUpdateDiscordUser] user did exist:', user)
  }

  user.set('accessToken', authData.access_token)
  user.set('refreshToken', authData.refresh_token)
  user.set('ethAddress', ethAddress)
  user.set('ethAddressVerified', false)
  user.set('tokenCreated', new Date())
  user.set('expiresIn', authData.expires_in)
  user.set('placedOrderId', moralisPlacedOrderId) // TODO consider relation https://www.notion.so/PHASE-3-7294a73865c84438a45bb1e31d9faef4?p=44b6cdeb540b4d26b45dd899d0dfa0a8
  // log('[discord] saving user:', user)
  await user.save()
  log(`[discord] saved user: ${user.id}`)
  return user
}
