import { sendToDiscord } from '../discord'
import { formatAddressShort, log } from '../utils'

const DISCORD_CONTRACT_INTERACTIONS = 'https://discord.com/api/webhooks/851572857732595782/8nNCO2aY1F0qhAa3QFCNZ7PBNDkFyhHyEWJWBCxrzqkWd_vAX4MlXjKRsdF6P48fBxir?wait=true'
const ETHERSCAN_RINKEBY = 'https://rinkeby.etherscan.io/tx/'

export function initDataHooks () {
  Moralis.Cloud.beforeConsume('Claims', (event) => {
  // TODO remove unwanted columns from table definition automatically
  // for now it is done manually in dashboard browser
    if (!event.confirmed || event.from !== '0x0000000000000000000000000000000000000000') return false
    delete event.from
    delete event.address
    delete event.confirmed
    delete event.block_hash
    log(`beforeConsume - ${event}`)
    return true
  })

  Moralis.Cloud.afterSave('Claims', async (request) => {
    const config = await Moralis.Config.get({ useMasterKey: true })
    const CHAIN_ID = config.get('CHAIN_ID')
    const VERITAS_CONTRACT = config.get('VERITAS_CONTRACT')
    const VERITAS_ABI = JSON.parse(config.get('VERITAS_ABI'))
    const web3 = Moralis.web3ByChain(CHAIN_ID)
    const contract = new web3.eth.Contract(VERITAS_ABI, VERITAS_CONTRACT)
    const claimer = request.object.get('to')
    const tokenId = request.object.get('tokenId')
    const balance = await contract.methods.balanceOf(claimer).call()
    const msg = `Claimed:  #${tokenId}\naddress:  ${formatAddressShort(claimer)}\nholding:  ${balance} VERITAS total\n==========`
    log(msg)
    await sendToDiscord(DISCORD_CONTRACT_INTERACTIONS, msg)
  })

  Moralis.Cloud.beforeConsume('Shipped', (event) => {
  // TODO remove unwanted columns from table definition automatically
  // for now it is done manually in dashboard browser
    if (!event.confirmed) return false
    delete event.address
    delete event.confirmed
    delete event.block_hash
    log(`beforeConsume - ${event}`)
    return true
  })

  Moralis.Cloud.afterSave('Shipped', async (request) => {
    const config = await Moralis.Config.get({ useMasterKey: true })
    const CHAIN_ID = config.get('CHAIN_ID')
    const VERITAS_CONTRACT = config.get('VERITAS_CONTRACT')
    const VERITAS_ABI = JSON.parse(config.get('VERITAS_ABI'))
    const web3 = Moralis.web3ByChain(CHAIN_ID)
    const txHash = request.object.get('transaction_hash')
    const tokenId = request.object.get('tokenId')

    // log(`${txHash.substr(0, 8)} - Shipped Record : ${JSON.stringify(request, null, 2)}`)
    const txObj = await web3.eth.getTransaction(txHash)
    const shipper = txObj.from

    // log(`${shipper} - flipped Tx :${JSON.stringify(txObj, null, 2)}`)

    // TODO add etherscan link
    const etherscan = `${ETHERSCAN_RINKEBY}${txHash}`
    const contract = new web3.eth.Contract(VERITAS_ABI, VERITAS_CONTRACT)
    const balance = await contract.methods.balanceOf(shipper).call()
    const msg = `Shipped:  #${tokenId}\naddress:  ${formatAddressShort(shipper)}\nholding:  ${balance} VERITAS total\n[etherscan link](${etherscan})\n==========`
    log(msg)
    await sendToDiscord(DISCORD_CONTRACT_INTERACTIONS, msg)
  })
}
