This project is a fork of Unisocks, updated to Uniswap v2 and tested on ETH Rinkeby testnet.

It was not fully launched on any mainnets, and is now archived.

Development continues on:  
https://gitlab.com/onezoomin/regen-grants/wineswap-evm
